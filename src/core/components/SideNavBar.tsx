import React, { useState } from "react";
import { Link } from "react-router-dom";
import { DynamicRoutes } from "./DynamicRoutes.ts";
import { AllRoutesService } from "../../Services/adminServices/AllRoutes.ts";

const SideNavBar = ({ openBox, setOpenBox }) => {
  const [state, setState] = useState<DynamicRoutes>({
    routes: AllRoutesService.getAllRoutes(),
  });

  const [open, setOpen] = useState(true);
  const [changeActive, setChangeActive] = useState("Home");

  return (
    <aside
      className={
        openBox
          ? "sidebar-wrapper custom-scrollbar wow fadeInLeft open"
          : "sidebar-wrapper  custom-scrollbar wow fadeInLeft"
      }
    > 
      <div className="sidebar-content-wrapper">
        <ul className="sidebar-list">
          <li
            className={
              open
                ? "sidebar-list-item has-subnav active open"
                : "sidebar-list-item has-subnav active "
            }
            id="listTem"
          >
            <ul>
              {state.routes.map((route, index) => (
                <li key={index} onClick={() => setChangeActive(route.label)}>
                  <Link
                    to={route.path}
                    className={`sidebar-link ${changeActive === route.label ? 'active' : ''}`}
                  >
                    {route.label}
                  </Link>
                </li>
              ))}
            </ul>
          </li>
        </ul>
      </div>
    </aside>
  );
};

export default SideNavBar;
