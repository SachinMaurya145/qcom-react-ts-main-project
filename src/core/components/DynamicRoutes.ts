export interface DynamicRoutes {
  path: string;
  label: string;
}

export interface Student {
  id: any;
  name: string;
  age: string;
}
