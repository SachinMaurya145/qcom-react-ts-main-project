import { DynamicRoutes } from "../../core/components/DynamicRoutes";

export class AllRoutesService {
    private static routes: DynamicRoutes[] = [
        { path: '/home', label: 'Home' },
        { path: '/about', label: 'About' },
        { path: '/profile', label: 'Profile' },
        { path: '/setting', label: 'Setting' },
    ];

    public static getAllRoutes() {
        return this.routes;
    }
}

