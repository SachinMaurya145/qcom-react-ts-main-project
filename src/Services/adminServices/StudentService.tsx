export const getStudents = () => {
  const savedStudentsJSON = localStorage.getItem('students');
  return savedStudentsJSON ? JSON.parse(savedStudentsJSON) : [];
};

export const addStudent = (student) => {
  console.log(" comming Data", student);
  const students = getStudents();
  student.id = Date.now();
  students.push(student);
  localStorage.setItem('students', JSON.stringify(students));
  return students;
}; 

export const editStudent = (updatedStudent) => {
  const students = getStudents();
  const updatedStudents = students.map((student) => {
    if (student.id === updatedStudent.id) {
      return {
        ...student,
        name: updatedStudent.name,
        age: updatedStudent.age,
      };
    } else {
      return student;
    }
  });
  localStorage.setItem('students', JSON.stringify(updatedStudents));
  return updatedStudents;
};
