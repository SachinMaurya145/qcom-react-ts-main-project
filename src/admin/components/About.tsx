import React, { useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { Student } from '../../core/components/DynamicRoutes';
import { getStudents, addStudent, editStudent } from '../../Services/adminServices/StudentService.tsx';

function About() {
  const [show, setShow] = useState(false);
  const [students, setStudents] = useState<Student[]>([]);
  const [isEditing, setIsEditing] = useState(false);
  const [newStudent, setNewStudent] = useState<Student>({
    id: 0,
    name: '',
    age: '',
  });

  const handleClose = () => {
    setShow(false);
    setIsEditing(false);
  };

  useEffect(() => {
    const students = getStudents();
    setStudents(students);
  }, []);

  const handleShow = () => setShow(true);

  const handleAddNewStudent = () => {
    setIsEditing(false);
    setNewStudent({  id:'',
      name: '',
      age: '', });
    handleShow();
  };

  const handleAddStudent = () => {
    const { name, age } = newStudent;

    if (name.trim() === '' && age.trim() === '') {
    } else {
      let updatedStudents;
      if (isEditing) {
         updatedStudents = editStudent(newStudent);
      } else {
       updatedStudents = addStudent(newStudent);
      }
      setStudents(updatedStudents);
      setNewStudent({
        id: 0,
        name: '',
        age: '',
      });
      handleClose();
    }
  }

  const handleEditStudent = (student) => {
    setNewStudent(student);
    setIsEditing(true);
    handleShow();
  };


return (
  <div className="container">
    <div className="mt-3 mb-4">
      <button onClick={handleAddNewStudent}>
        Add New Data
      </button>
    </div>
    <div className="table-responsive">
      <table className="table table-hover table-bordered">
        <thead>
          <tr>
            <th>Name</th>
            <th>Age</th>
            <th>Edit Data</th>
          </tr>
        </thead>
        <tbody>
          {students?.map((student) => (
            <tr key={student.id}>
              <td>{student.name}</td>
              <td>{student.age}</td>
              <td>
                  <a href="#" className="edit" title="Edit" data-toggle="tooltip" style={{ marginRight: '10px' }} onClick={() => handleEditStudent(student)}>
                    Edit
                  </a>
                </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>

    <div className="model_box">
      <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
        <Modal.Header closeButton>
        <Modal.Title>{isEditing ? "Edit Student" : "Add Student"}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                placeholder="Enter Name"
                value={newStudent.name}
                onChange={(e) => setNewStudent({ ...newStudent, name: e.target.value })}
              />
            </div>
            <div className="form-group mt-3">
              <input
                type="text"
                className="form-control"
                placeholder="Enter Age"
                value={newStudent.age}
                onChange={(e) => setNewStudent({ ...newStudent, age: e.target.value })}
              />
            </div>

            <button type="button" className="btn btn-success mt-4" onClick={handleAddStudent}>
                {isEditing ? "Save Changes" : "Add"}
              </button>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  </div>
);
}

export default About;
