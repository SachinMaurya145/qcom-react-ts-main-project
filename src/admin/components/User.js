import React from 'react';
import { useParams } from 'react-router-dom';

function User() {
  let { userId } = useParams();

  return (
    <div>
      <h2>User Profile</h2>
      <p>User ID: {userId}</p>
    </div>
  );
}

export default User;
