import React, { useState } from 'react';

import SideNavBar from '../../core/components/SideNavBar.tsx';
import Footer from './Footer';
import TopBar from './TopBar';
import { Outlet } from 'react-router-dom';


const AdminDashboard = () => {
  const [open,setOpen]= useState(true);

  return (
    <div>
    <TopBar open={open} setOpen={setOpen}/>
    <div className="page-wrapper">
        <SideNavBar  openBox={open} setOpenBox={setOpen}/>
       <div className="content-area-wrapper">
          <Outlet/>
         <Footer/>
       </div>
    </div>
 </div>
  )
}

export default AdminDashboard;
