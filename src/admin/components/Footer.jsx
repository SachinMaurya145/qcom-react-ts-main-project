import React from 'react'

const Footer = () => {
  return (
    <footer className="site_footer">
      <div className="copy-text-wrapper"> All rights reserved.</div>
      <div className="footer-links-wrapper"><a>Terms &amp; Conditions </a> <span className="mx-2">|</span><a>Privacy Policy </a></div> 
    </footer>
  )
}

export default Footer;
