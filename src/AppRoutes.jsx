import React from "react";
import { Route, Routes } from 'react-router-dom';

// admin
import AdminDashboard from "./admin/components/AdminDashboard.tsx";
import Home from "./admin/components/Home";
import About from "./admin/components/About.tsx";
import Profile from "./admin/components/Profile";
import Setting from "./admin/components/Setting";


// DefaultPage 
import DefaultPage from "./default/components/DefaultPage";

const AppRoutes = () => {


    return (
        <Routes>
            <Route path="/" element={<AdminDashboard />}>
                <Route path="home" element={<Home />} />
                <Route path="about" element={<About />} />
                <Route path="/profile" element={<Profile />} />
                <Route path="/setting" element={<Setting />} />
                <Route path="*" element={<DefaultPage />} />
            </Route>

        </Routes>
    )
}

export default AppRoutes;
